const express = require("express");
const router = express.Router();
const experience = require("../controllers/experience");
const {
    getAllTransportTypes
} = require("../controllers/transportTypeController");
const {
    getAllPublicTransports,getByTransportType
} = require("../controllers/publicTransportController");
const {
    getAllStations,getStationsByPublicTransport
} = require("../controllers/stationController");
const {
    addUserDetail
} = require("../controllers/userDetailController");
const {
    getUserDetail,getUser,checkUserLogin
} = require("../controllers/userDetailController");
const {
    updateUserDetail
} = require("../controllers/userDetailController");
const {updateUserPassword}=require("../controllers/userDetailController");
const {deleteExperience}=require("../controllers/experience")
const{getAllExperiences}=require("../controllers/experience")

router.post("/add", experience.addExperience);
router.get("/getalltransporttypes", getAllTransportTypes);
router.get("/getallpublictransports", getAllPublicTransports);
router.get("/getpublictransportsbytype/:id", getByTransportType);
router.get("/getallstations", getAllStations);
router.get("/getstationsbypublictransport/:id", getStationsByPublicTransport);

router.post("/adduserdetail", addUserDetail);
router.get("/getDetails/:id", getUserDetail);
router.get("/getUserLogin/:username", getUser);
router.put("/userdetail/:id", updateUserDetail)
router.put("/userdetail/password/:id",updateUserPassword);
router.put("/checklogin",checkUserLogin);
router.delete("/userdetail/:id",deleteExperience);

router.get("/experiences",getAllExperiences);

// get pe experience
router.get("/experiences/:id", experience.getExperiences);

//dam merge la asta cu userdetail
//router.post("/adduserauth", addUserAuth);

//se vor introduce local pe /reset dintr-un json extern
//router.post("/addtransporttype", addTransportType);
//router.post("/addpublictransport", addPublicTransport);
//router.post("/addstation", addStation);

module.exports = router;