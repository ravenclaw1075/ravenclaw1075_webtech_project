const express = require("express");
const router = express.Router();

router.use("/routes",require("./routes"));
router.use("/database",require("./db"))


module.exports=router;