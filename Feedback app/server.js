const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const router = require("./routes");
const {
    port
} = require("./config");
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());
app.use("/api", router);

app.listen(port, () => {
    console.log(`Server running on ${port}`);
});