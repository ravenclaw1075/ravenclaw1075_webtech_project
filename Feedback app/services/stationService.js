const sequelize = require('../config/db');
const Sequelize = require('sequelize');
const Station = require('./../models/stations')(sequelize, Sequelize.DataTypes);

const station = {
    create: async (station) => {
        try {
            const result = await Station.create(station);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAll: async () => {
        try {
            const stations = await Station.findAll();
            return stations;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getStationsByPublicTransport: async (idPublicTransport) => {
        try {
            const stations = await Station.findAll({
               where:{ publictransportId: idPublicTransport}
            });
            return stations;
        } catch (err) {
            throw new Error(err.message);
        }
    }
}

module.exports = station;