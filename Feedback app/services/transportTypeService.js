const sequelize = require('../config/db');
const Sequelize = require('sequelize');
const TransportType = require('./../models/transportType')(sequelize, Sequelize.DataTypes);

const transportType = {
    create: async (transportType) => {
        try {
            const result = await TransportType.create(transportType);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAll: async () => {
        try {
            const transportTypes = await TransportType.findAll();
            return transportTypes;
        } catch (err) {
            throw new Error(err.message);
        }
    }
}

module.exports = transportType;