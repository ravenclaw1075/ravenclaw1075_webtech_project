const sequelize = require('../config/db');
const Sequelize = require('sequelize');

const PublicTransport = require('./../models/publicTransport')(sequelize, Sequelize.DataTypes);

const publicTransport = {
    create: async (publicTransport) => {
        try {
            const result = await PublicTransport.create(publicTransport);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAll: async () => {
        try {
            const publicTransports = await PublicTransport.findAll();
            return publicTransports;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getByTransportType: async (idTransportType) => {
        try {
            const publicTransports = await PublicTransport.findAll({
                where: {transporttypeId: idTransportType}
            });
            return publicTransports;
        } catch (err) {
            throw new Error(err.message);
        }
    }
}

module.exports = publicTransport;