const sequelize = require('../config/db');
const Sequelize = require('sequelize');
const UserDetail = require('./../models/userDetail')(sequelize, Sequelize.DataTypes);

const userDetail = {
    create: async (userDetail) => {
        try {
            const result = await UserDetail.create(userDetail);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getUser: async (id1) => {
        try {
            const user = await UserDetail.findOne({
                where: {
                    id: id1
                }
            })
            return user
        } catch (err) {
            throw new Error(err.message)
        }
    },
     getUserByUsername: async (username) => {
        try {
            const user = await UserDetail.findOne({
                where: {
                    username: username
                }
            })
            return user
        } catch (err) {
            throw new Error(err.message)
        }
    },
    updateUser: async (user,idUser) => {
        try {
            const result = await UserDetail.update({
                FirstName: user.FirstName,
                LastName: user.LastName
            }, {
                where: {
                    id: idUser
                }
            })
            return result
        } catch (err) {
            throw new Error(err.message)
        }
    },
    changePassword: async(newPassword,idUser)=>{
        try{
                const result=await UserDetail.update({
                    password:newPassword
                },{
                    where:{
                        id:idUser
                    }
                })
                return result
        }catch(ex){
            throw new Error(ex.message)
        }
    }
}

module.exports = userDetail;