const sequelize = require('../config/db');
const Sequelize = require('sequelize');
const
    Experience = require('./../models/experience')(sequelize, Sequelize.DataTypes);

const experience = {
    addExperience: async (experience) => {
        console.log(experience)
        try {
            const result = await Experience.create(experience);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAllByUserId: async (id) => {

        try {
            const experiences = await Experience.findAll({
                  where:  {userdetailId: id}
            });
            return experiences;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    deleteExperience:async(userId)=>{
        try{
            const result= await Experience.destroy({
                where:{
                    id:userId
                }
            })
            return result
        }catch(ex){
            throw new Error(ex.message)
        }
    },
    getAll:async()=>{
        try{
            const experiences=await Experience.findAll();
            return experiences
        }catch(ex){
            throw new Error(ex.message)
        }
    }
}

module.exports =
    experience;