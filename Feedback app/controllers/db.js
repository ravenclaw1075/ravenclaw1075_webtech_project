const fs = require('fs');
const sequelize = require('../config/db');
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

const userModel = require("../models/userDetail")(sequelize,DataTypes)
const transportTypesModel = require("../models/transportType")(sequelize,DataTypes)
const publicTransportModel = require("../models/publicTransport")(sequelize,DataTypes)
const stationsModel = require("../models/stations")(sequelize,DataTypes)
const {
    db
} = require("../models/index");
const {
    database
} = require("../configuration").development;

const mysql = require('mysql');


const initialFile = fs.readFileSync("./initialData.json");
  const data = JSON.parse(initialFile);
  const completeJson = data[0];
  //console.log(completeJson.transporttypes)
  const transporttypes = completeJson.transporttypes,
    publictransport = completeJson.publictransport,
    stations = completeJson.stations;



var con = mysql.createConnection({
    host: database.host,
    user: database.username,
    password: database.password
});

con.connect((err) => {
    if (err) throw err;
});

const controller = {
    reset: async (req, res) => {
        try {
            await con.query('SET FOREIGN_KEY_CHECKS = 0', async () => {
                await db.sync({
                    force: true
                });
                await transportTypesModel.bulkCreate(transporttypes)
                 await publicTransportModel.bulkCreate(publictransport)
                 await stationsModel.bulkCreate(stations);
                 await userModel.create({
         FirstName: "Anonymous",
         LastName: "N/A",
         email: "anonymousMail@gmail.com",
         birthdate: "N/A",
         username: "anonymous",
         password: "1234"
        })
                res.status(201).send({
                    message: "Database reset"
                });
            });
            
        } catch (e) {
            console.error(e);
            res.status(500).send({
                message: "Database error"
            });
        }
    }
};

module.exports = controller;