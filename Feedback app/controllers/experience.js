const experienceService = require("../services/experienceService");


const addExperience = async (req, res) => {
    const experience = req.body;
    if (experience.satisfactionLevel != 0){ //&& experience.transporttype!=null  &&
  //  experience.publictransport!="" && experience.crowdnessLevel!=0 && experience.startingPoint!=null &&
   // experience.destinationPoint!="" && experience.time!="" && experience.duration!="" &&
  //  experience.userdetailId!=0 && experience.transporttype!=0) {//validari pt toate
        console.log(experience)
        await experienceService.addExperience(experience);
        res.status(201).send({
            message: 'Experience added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid experience payload.'
        });
    }
};


const getExperiences = async (req, res) => {
    const id = req.params.id
    const experiences = await experienceService.getAllByUserId(id);
    if (experiences) {
        res.status(200).send(experiences);
    } else {
        res.status(500).send({
            message: "Error while getting experiences"
        })
    }
}

const deleteExperience=async(req,res)=>{
    const id=req.params.id
    const experience= await experienceService.deleteExperience(id)
    
    if(experience){
        res.status(200).send({
            message:"Experience deleted"
        })
    }else{
        res.status(500).send({
            message:"There has been an error while deleting an experience"
        })
    }
}

const getAllExperiences=async(req,res)=>{
    const experiences = await experienceService.getAll();
    if(experiences){
        res.status(200).send(experiences)
    }else{
        res.status(500).send({
            message:"There has been an error while retriving the experiences"
        })
    }
}


module.exports = {
    addExperience,
    getExperiences,
    deleteExperience,
    getAllExperiences
};