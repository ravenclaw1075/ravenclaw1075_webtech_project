const express = require("express");
const publicTransportService = require('../services/publicTransportService');
const PublicTransport = require("../models/publicTransport");

//    const addPublicTransport = async (req,res,next)=>{ 
//         const publicTransport = req.body;
//     if(publicTransport.number && publicTransport.transporttypeId) {
//         const result = await publicTransportService.create(publicTransport);
//         res.status(201).send({
//             message: 'Transport Type added successfully.'
//         });
//     } else {
//         res.status(400).send({
//             message: 'Invalid Transport Type payload.'
//         });
//     }
//    };

const getAllPublicTransports = async (req, res, next) => {
    try {
        const publicTransports = await publicTransportService.getAll();
        res.status(200).send(publicTransports);
    } catch (err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }

};

const getByTransportType = async (req, res, next) => {
    try {
        const transporttypeId = req.params.id;
        if (transporttypeId) {
            try {
                const publicTransports = await publicTransportService.getByTransportType(transporttypeId);
                res.status(200).send(publicTransports);
            } catch (err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No transport type id specified'
            })
        }
    } catch (err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

module.exports = {
    // addPublicTransport,
    getAllPublicTransports,
    getByTransportType
}