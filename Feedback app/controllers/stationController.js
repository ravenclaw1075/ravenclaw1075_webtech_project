const express = require("express");
const stationService = require('../services/stationService');



const getAllStations = async (req, res, next) => {
    try {
        const publicTransports = await stationService.getAll();
        res.status(200).send(publicTransports);
    } catch (err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }

};

const getStationsByPublicTransport = async (req, res, next) => {
    try {
        const publicTransportId = req.params.id;
        if (publicTransportId) {
            try {
                const stations = await stationService.getStationsByPublicTransport(publicTransportId);
                res.status(200).send(stations);
            } catch (err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No public transport id specified'
            })
        }
    } catch (err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

module.exports = {
    // addStation,
    getAllStations,
    getStationsByPublicTransport
}