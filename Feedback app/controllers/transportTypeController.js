const express = require("express");
const TransportType = require("../models/transportType");
const transportTypeService = require('../services/transportTypeService');


const getAllTransportTypes = async (req, res, next) => {

    const transportTypes = await transportTypeService.getAll();
    res.status(200).send(transportTypes);


};

module.exports = {
    getAllTransportTypes
}