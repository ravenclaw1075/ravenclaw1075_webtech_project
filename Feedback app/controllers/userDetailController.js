const userDetailService = require('../services/userDetailService');


const addUserDetail = async (req, res, next) => {
    const userDetail = req.body;

    if (userDetail.FirstName && userDetail.LastName && userDetail.birthDate) {
        await userDetailService.create(userDetail);
        res.status(201).send({
            message: 'User Detail added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid User Detail payload.'
        });
    }
};

const getUserDetail = async (req, res) => {
    const id = req.params.id;
    if (id) {
        const user = await userDetailService.getUser(id);
        res.status(200).send(user)
    }
}
const getUser = async (req, res) => {
    const username = req.params.username;
    if (username) {
        const user = await userDetailService.getUserByUsername(username);
        if(user)
            res.status(200).send(user);
        else
            res.status(500).send({
                succes:0
            });
    }
}
const updateUserDetail = async (req, res) => {
    const user = req.body;
    const id = req.params.id;
    if (id) {
        const result = await userDetailService.updateUser(user, id)
        res.status(200).send(user)
    }
}

const updateUserPassword = async(req,res)=>{
    const user=req.body;
    const id=req.params.id;
    if(id){
        const result = await userDetailService.changePassword(user.password,id)
        res.status(200).send({
            message:"Password changed"
        })
    }
}

const checkUserLogin= async (req, res) => {
    const user = req.body;
    if (user.username) {
        const result = await userDetailService.getUserByUsername(user.username)
        if(result.password != user.password)
            res.status(200).send({message: 'wrong'});
        else res.status(200).send(result);
    }
}


module.exports = {
    addUserDetail,
    getUserDetail,
    updateUserDetail,
    updateUserPassword,
    getUser,
    checkUserLogin
}