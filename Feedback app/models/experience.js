const userDetail = require("./userDetail")
const transportType = require("./transportType")

module.exports = (sequelize, DataTypes) => {
    return sequelize.define("experience", {
        transportType:DataTypes.STRING,
        publicTransport:DataTypes.STRING,
        satisfactionLevel: DataTypes.INTEGER,
        crowdnessLevel: DataTypes.INTEGER,
        startingPoint: DataTypes.STRING,
        destinationPoint: DataTypes.STRING,
        time:DataTypes.STRING,
        duration:DataTypes.INTEGER,
        observations: DataTypes.STRING,
        userdetailId: {
            type: DataTypes.INTEGER


        },
        transporttypeId: {
            type: DataTypes.INTEGER

        }
    });
};