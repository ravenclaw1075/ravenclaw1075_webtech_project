const db = require("../config/db");
const Experience = db.import("./experience");
const UserDetail = db.import("./userDetail");
const PublicTransport = db.import("./publicTransport");
const TransportType = db.import("./transportType");
const Station = db.import("./stations");


PublicTransport.belongsTo(TransportType, {
    onDelete: "Cascade"
});

Station.belongsTo(PublicTransport, {
    onDelete: "Cascade"
});

Experience.belongsTo(UserDetail, {
    targetKey: "id",
    constraints: false,
    onDelete: "Cascade"
});

Experience.belongsTo(TransportType, {
    onDelete: "Cascade"
});

module.exports = {
    db,
    TransportType,
    Experience,
    UserDetail,
    PublicTransport,
    Station
}