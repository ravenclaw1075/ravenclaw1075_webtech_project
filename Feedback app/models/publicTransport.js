module.exports = (sequelize, DataTypes) => {
    return sequelize.define("publictransport", {
        number: DataTypes.STRING,
        transporttypeId: {
            type: DataTypes.INTEGER
        }
    });
};