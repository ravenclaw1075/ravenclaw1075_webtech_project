module.exports = (sequelize, DataTypes) => {
    return sequelize.define("userdetail", {
        FirstName: DataTypes.STRING,
        LastName: DataTypes.STRING,
        email: DataTypes.STRING,
        birthDate: DataTypes.STRING,
        username: DataTypes.STRING,
        password: DataTypes.STRING
    });
};