import React from 'react';
import Table from 'react-bootstrap/Table'
import axios from 'axios';
import $ from "jquery";
import './Experiences.css'
import { FaTrashAlt } from "react-icons/fa";


class ExperiencesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            experiences: [],
            userdetailId:''
        };
    }
    
  deleteRow= function(e,i) {
      var experiences = [...this.state.experiences];
      console.log(e);
        experiences.splice(i, 1);
        console.log(experiences);
        this.setState({experiences});
        
        axios.delete('http://3.14.28.204:8080/api/routes/userdetail/'+e).then(items=>{
        
        });
        
    }
    
    handleDeleteButtonClick = (e,i) => {
     this.deleteRow(e,i)
    }


 componentWillMount(){
        const { id } = this.props.match.params
        this.state.userdetailId=id;
    }
   componentDidMount =()=>{


        axios.get('http://3.14.28.204:8080/api/routes/experiences/'+this.state.userdetailId).then(items=>{


            this.setState({
                experiences:items.data
            })
        })

     
    $(document).ready(function(){
  $("#searchType").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    let table = document.getElementById("experienceTable");
     let tr = table.getElementsByTagName("tr");
    for(let i=1 ;i < tr.length;i++){
    let td=tr[i];
    $(td).filter(function() {
        let found=0;
        let searchable=td.getElementsByClassName('searchable');
        $(searchable).filter(function(){
           if($(this).text().toLowerCase().indexOf(value) > -1) 
             found=1;
        });
      if(found===0)
      {  
          $(td).hide();
      }
      else
      {
          $(td).show();
      }

    });
    }
     
  });
});

   }
    
    render() {
        const experiences = () => {

            return this.state.experiences.map( (experience, i) => { return(
                <tr key={i}>
                    <td className="searchable">{experience.transportType}</td>
                    <td className="searchable">{experience.publicTransport}</td>
                    <td className="unsearchable">{experience.satisfactionLevel}</td>
                    <td className="unsearchable">{experience.crowdnessLevel}</td>
                    <td className="unsearchable">{experience.time}</td>
                    <td className="unsearchable">{experience.duration}</td>
                    <td className="searchable">{experience.startingPoint}</td>
                    <td className="searchable">{experience.destinationPoint}</td>
                    <td className="unsearchable">{experience.observations}</td>
                    <td className="unsearchable"><button className="btnDelete" id="button" onClick ={()=>this.handleDeleteButtonClick(this.state.experiences[i].id,i)}
                    ><FaTrashAlt/></button></td>
                </tr>
            )});
        };
  

        return (
            <div>
             
               <div className="form-group">
                    <input className="form-control search" placeholder="Search" type="text"
                        id="searchType" />
                </div>
                <Table responsive striped bordered hover variant="dark"
                id="experienceTable" style={{width:'fit-content', background:'rgba(3,24,45,0.8)'}}>
                   
                    <thead>
                        <tr>
                             <th>Transport Type</th>
                             <th>Public Transport</th>
                             <th>Satisfaction Level</th>
                             <th>Crowdness Level</th>
                             <th>Time</th>
                             <th>Duration</th>
                             <th>Starting Point</th>
                             <th>Destination Point</th>
                             <th>Observations</th>
                             <th></th>
                        </tr>
                        
                     </thead>
                     <tbody>
                         {experiences()}
                    
                    </tbody>
                   
                </Table>
               
            </div>
        );
    }
}

export default ExperiencesList;