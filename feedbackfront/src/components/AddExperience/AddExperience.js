import React from 'react';
import './AddExperience.css'
import axios from 'axios';




class AddExperience extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            satisfactionLevel:1,
            crowdnessLevel: 1,
            startingPoint:'',
            destinationPoint:'',
            time:'',
            duration:'',
            observations:'',
            transportType:'',
            transportTypeId:'',
            publicTransport:'',
            publicTransportId:'',
            stationId:'',
            transportTypes:[],
            publicTransports:[],
            startingPoints:[],
            destinationPoints:[],
            userdetailId:''
        };
    }
    componentWillMount(){
        const { id } = this.props.match.params
        this.state.userdetailId=id;
    }
    handleChangeSatisfactionLevel = (event) => {
        this.setState({
            satisfactionLevel: event.target.value
        });
    }
    handleChangeCrowdnessLevelCategory = (event) => {
        this.setState({
            crowdnessLevel: event.target.value
        });
    }
    
    handleChangeTime = (event) => {
        this.setState({
            time:event.target.value
        });
    }
     handleChangeDuration = (event) => {
        this.setState({
            duration: event.target.value
        });
    }
    
    
    handleChangeStartingPoint = (event) => {
        this.setState({
            startingPoint: event.target.value
        });
        let name=event.target.value;
        console.log(name);
        let array=this.state.startingPoints.filter(item=>{if((item.name!=name)===true) return item});
        console.log(array);
        this.setState({
            destinationPoints:array
        });
    }
    handleChangeDestinationPoint = (event) => {
        this.setState({
            destinationPoint: event.target.value
        });
    }
    handleChangeTransportType = (event) => {
        let sel=event.target;
        let text= sel.options[sel.selectedIndex].text;
        this.setState({
            transportTypeId: event.target.value,
            transportType:text
        });

        axios.get('http://3.14.28.204:8080/api/routes/getpublictransportsbytype/'+event.target.value).then(items=>{

            this.setState({
                publicTransports:items.data
            });
        });
        this.setState({
                startingPoints:[],
                destinationPoint:[]
            });
        
    }
    handleChangePublicTransport = (event) => {
        let sel=event.target;
        console.log(this.state);
        let text= sel.options[sel.selectedIndex].text;
        this.setState({
            publicTransportId: event.target.value,
            publicTransport:text
        });


        axios.get('http://3.14.28.204:8080/api/routes/getstationsbypublictransport/'+event.target.value).then(items=>{


            this.setState({
                startingPoints:items.data
            });
        });
       
    }
    handleChangeObservations = (event) => {
        this.setState({
            observations: event.target.value
        });
    }
    
    //get transport type
    componentDidMount =()=>{


        axios.get('http://3.14.28.204:8080/api/routes/getalltransporttypes').then(items=>{


            this.setState({
                transportTypes:items.data
            });
        });
    }
    //get public transport dupa transporttype id
    //get stations -> starting point toate
    //get stations fara starting point
    //validarile 
    
  //  asta pentru adaugare
    addExperince = () => {
        const experience = {
            transportType:this.state.transportType,
            publicTransport:this.state.publicTransport,
            satisfactionLevel: this.state.satisfactionLevel,
            crowdnessLevel:this.state.crowdnessLevel,
            startingPoint:this.state.startingPoint,
            destinationPoint:this.state.destinationPoint,
            time:this.state.time,
            duration:this.state.duration,
            observations:this.state.observations,
            userdetailId:this.state.userdetailId,
            transporttypeId:this.state.transportTypeId
            
        };

        //validari sa punem


        axios.post('http://3.14.28.204:8080/api/routes/add', experience).then(res => {
        
        }).catch(err => {
            console.log(err);
        });
    }
    render() {
        const transportTypes = () => {
            return this.state.transportTypes.map(item => (
                <option className="option-select" key={item.id} value={item.id} >
                    {item.typeName}
                </option>
            )
        )};
        const publicTransports = () => {
            return this.state.publicTransports.map(item => (
                <option className="option-select" key={item.id} value={item.id}>
                    {item.number}
                </option>
            )
        )};
        const startingPoints = () => {
            return this.state.startingPoints.map(item => (
                <option className="option-select" key={item.id} value={item.name}>
                    {item.name}
                </option>
            )
        )};
        const destinationPoints = () => {
            return this.state.destinationPoints.map(item => (
                <option className="option-select" key={item.id} value={item.name}>
                    {item.name}
                </option>
            )
        )};
        return (
             <div className="container form-container">
                <div className="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
          
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">New experience</h3>
                    </div>
                    <div className="panel-body">
                        <form>
                        
                        <div className="form-group">
                            <select className="form-control" placeholder="Choose transport type" name="transporttype"
                                autoFocus="" required
                                onChange={this.handleChangeTransportType}
                                value={this.state.transportTypeId}>
                                <option disabled={true} value="">Choose transport type</option>
                                {transportTypes()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control" placeholder="Choose public transport" name="publicTransport"
                                autoFocus="" required
                                onChange={this.handleChangePublicTransport}
                                value={this.state.publicTransportId}>
                                <option disabled={true} value="">Choose nr transport</option>
                                {publicTransports()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control"
                            value={this.state.startingPoint}
                            onChange={this.handleChangeStartingPoint}>
                            <option disabled={true} value="">Start</option>
                                {startingPoints()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control"
                            value={this.state.destinationPoint}
                            onChange={this.handleChangeDestinationPoint}>
                            <option disabled={true} value="">Destination</option>
                                {destinationPoints()}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="satisfactionLevel">Satisfaction Level:</label>
                            <input className="form-control"
                            type="range"
                            name="satisfactionLevel"
                            max="5"
                            min="1"
                            onChange={this.handleChangeSatisfactionLevel}
                            value={this.state.satisfactionLevel} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="crowdnessLevel">Crowdness Level:</label>
                            <input className="form-control"
                            type="range"
                            name="crowdnessLevel"
                            max="5"
                            min="1"
                            onChange={this.handleChangeCrowdnessLevelCategory}
                            value={this.state.crowdnessLevel} />
                        </div>
                       
                        <div className="form-group">
                            <label htmlFor="time">Time </label>
                            <input className="form-control"
                            type="time"
                            id="time"
                            onChange={this.handleChangeTime}
                            value={this.state.time} />
                               
                         </div>
                         <div className="form-group">
                            <label htmlFor="duration">Duration (minutes) </label>
                            <input className="form-control" id="duration" 
                            type="text"
                            onChange={this.handleChangeDuration}
                            value={this.state.duration} ></input>
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="observations">Observations</label>
                            <textarea className="form-control" id="observations" required
                            onChange={this.handleChangeObservations}
                            value={this.state.observations} rows="3"></textarea>
                        </div>
                        
                        <button className="add-btn" onClick={this.addExperince}>Add experience</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
            
        );
    }
}

export default AddExperience;