import React from 'react';
import './UserProfile.css';
import axios from 'axios';
import ChangePassword from './ChangePassword'
import { withRouter } from "react-router-dom";

class UserProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id:'',
            firstName: '',
            lastName: '',
            email:'',
            birthDate:'',
            username:''
        };
    }
    componentWillMount(){
        const { id } = this.props.match.params
        this.state.id=id;
    }
    
    componentDidMount =()=>{
        axios.get('http://3.14.28.204:8080/api/routes/getDetails/'+this.state.id).then(items=>{
            this.setState({
                firstName:items.data.FirstName,
                lastName:items.data.LastName,
                email:items.data.email,
                birthDate:items.data.birthDate,
                username:items.data.username
            })
             console.log(items.data);
           
        })
    };
    handleChangeFirstName = (event) => {
        this.setState({
            firstName: event.target.value
        });
    }
    handleChangeLastName=(event)=>{
        this.setState({
           lastName:event.target.value 
        });
    }
  
    
    save = () => {
        const user={
                id:this.state.id, 
                FirstName:this.state.firstName,
                LastName:this.state.lastName,
                email:this.state.email,
                birthDate:this.state.birthDate
            }
            axios.put('http://3.14.28.204:8080/api/routes/userdetail/'+this.state.id, user).then(res => {
           
        }).catch(err => {
            console.log(err);
        })
         this.props.history.push(`/myprofile/${user.id}`);

    };
    
    change=()=>{
        this.props.history.push(`/changepassword/${this.state.id}`);
    };
    
        
    render() {
        const user={firstName:this.state.firstName,
            lastName:this.state.lastName,
            email:this.state.email,
            username:this.state.username,
            birthDate:this.state.birthDate
        };
    console.log(this.state)
        return (
            <div className="container profile">
            <div className="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                <div className="panel">
                    <div className="panel-heading">
                        <h3 className="panel-title">My profile</h3>
                    </div>
                    <div className="panel-body">
                        <form>
                            <div className="form-group">
                                <input className="form-control" placeholder="First Name" name="FirstName" type="text"
                                    autoFocus="" onChange={this.handleChangeFirstName} value={user.firstName} />
                            </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="Last Name" name="LastName" type="text"
                                autoFocus="" onChange={this.handleChangeLastName} value={user.lastName} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" readOnly value={user.email} name="email" type="email"
                            disabled/>
                        </div>
                        <div className="form-group">
                            <input className="form-control" readOnly value={user.birthDate} name="birthDate" type="text"
                            />
                        </div>
                        
                        <div className="form-group">
                            <input className="form-control" readOnly name="username" type="text" value={user.username}/>
                        </div>
                        
                        
                        <button className="submit-btn" onClick={this.save}>Save changes</button>
                        <button className="submit-btn passChange" onClick={this.change}>Change password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
           
        );
    }
}
export default withRouter(UserProfile);