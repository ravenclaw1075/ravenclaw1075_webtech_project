import React from 'react';
import './UserProfile.css';
import axios from 'axios';
import FormErrors from '../FormErrors/FormErrors';

class ChangePassword extends React.Component {
    state={
        userId:'',
        password:'',
        oldpassword:'',
        newpassword:'',
        confirmpassword:'',
        formErrors: {oldpassword: '', newpassword:'', confirmpassword:''},
            oldpasswordValid: false,
            newpasswordValid: false,
            confirmPasswordValid:false,
            formValid: false
    }
    componentWillMount(){
        const { id } = this.props.match.params
        this.state.userId=id;
    }
    componentDidMount(){
        axios.get('http://3.14.28.204:8080/api/routes/getDetails/'+this.state.userId).then(items=>{
            this.setState({
                password:items.data.password
            })
            console.log(items.data);
        });
    }
    validateField(fieldName, value) {
        let password=this.state.password;
        let newpassword=this.state.newpassword;
        let fieldValidationErrors = this.state.formErrors;
        let oldpasswordValid = this.state.oldpasswordValid;
        let newpasswordValid = this.state.newpasswordValid;
        let confirmPasswordValid=this.state.confirmPasswordValid;
        switch(fieldName) {
            case 'oldpassword':
                oldpasswordValid = value===password;
                fieldValidationErrors.oldpassword = oldpasswordValid ? '': ' not correctly introduced';
            break;
             case 'newpassword':
                newpasswordValid = value.length >= 6;
                fieldValidationErrors.newpassword = newpasswordValid ? '': ' is too short';
            break;
            case'confirmpassword':
                confirmPasswordValid=value.length>=6 && value===newpassword;
                fieldValidationErrors.confirmpassword=confirmPasswordValid? '':'Passwords not matching';
                break;
                
            default: break;
        }
        
        this.setState({formErrors: fieldValidationErrors,
                  oldpasswordValid:oldpasswordValid,
                  newpasswordValid: newpasswordValid,
                  confirmPasswordValid: confirmPasswordValid
            }, this.validateForm);
    }
    
    validateForm() {
        this.setState({formValid: this.state.oldpasswordValid && this.state.newpasswordValid && this.state.confirmPasswordValid});
    }
    handleValidationInput =(event)=> {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value}, 
                () => { this.validateField(name, value) });
    }
    
    saveChanges=()=>{
        const user = {
            id:this.state.userId,
            password:this.state.newpassword
        }
        console.log(user);
            axios.put('http://3.14.28.204:8080/api/routes/userdetail/password/'+this.state.userId, user).then(res => {
           
        }).catch(err => {
            console.log(err);
        })
         this.props.history.push(`/myprofile/${user.id}`);
    }
        
    render() {
      
        return (
        <div className="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
         <div className='panel'>
                    <div className="panel-heading">
                        <h3 className="panel-title">Change password</h3>
                    </div>
                    <div className="panel-body">
                    <div className="panel panel-default" style={{color:'red'}}>
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                        <form>
                        <div className="form-group">
                            <input className="form-control" placeholder="Old Password" name="oldpassword" 
                            type="password"  onChange={this.handleValidationInput}
                            value={this.state.oldpassword} required
                            />
                        </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="NewPassword" name="newpassword" 
                            type="password" onChange={this.handleValidationInput}
                            value={this.state.newpassword}  required
                            />
                        </div>
                         <div className="form-group">
                            <input className="form-control" placeholder="Confirm New Password" name="confirmpassword" 
                            type="password" onChange={this.handleValidationInput}
                            value={this.state.confirmpassword}  required
                            />
                        </div>
                        <button className="submit-btn" onClick={this.saveChanges}>Save</button>
                    </form>
                </div>
            </div>
            </div>
           
        );
    }
}
export default ChangePassword;