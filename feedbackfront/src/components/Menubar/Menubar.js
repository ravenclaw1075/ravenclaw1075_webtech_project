import React from 'react'
import './Menubar.css'
import { FaHome } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { FaClipboardList } from "react-icons/fa";
import { MdAddBox } from "react-icons/md";
import Login from '../Auth/login'
import { Link } from "react-router-dom";

class Menubar extends React.Component {
    
     
   myProfile(){	
       var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
	return window.location.href = "/myprofile/" + id;
   }
   experiences(){	
     var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
	return window.location.href = "/experiences/" + id;
   }
   addexp(){	
       var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
	return window.location.href = "/addexperience/" + id;
   }
    render() {
        let drawer='menubar';
        if(this.props.show){
            drawer='menubar open'
        }
       var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        console.log(id);
        return (
            <nav className={drawer}>
                <div className="imageStyle">
                    <img src="https://img.pngio.com/png-transport-8-png-image-transport-png-images-1400_1050.png" alt="LogoApp"/>
                </div>
                <ul>
                    <li><FaUser/> <a onClick={this.myProfile} >My profile</a></li>
                    <li><FaClipboardList/> <a  onClick={this.experiences}>My experiences</a></li>
                    <li><MdAddBox/> <a onClick={this.addexp}>New experience</a></li>
                </ul>
                
            </nav>
        );
    }
}

export default Menubar;