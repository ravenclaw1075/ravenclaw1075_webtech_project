import React from 'react'
import './ToggleMenu.css'

class ToggleMenu extends React.Component {
    render() {
        return (
            <button className="menu_button" onClick={this.props.click}>
                <div className="toggle_line"/>
                <div className="toggle_line"/>
                <div className="toggle_line"/>
            </button>
        );
    }
}

export default ToggleMenu;