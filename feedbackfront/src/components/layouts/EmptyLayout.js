import React from 'react';
import Background from '../Background/Background';

class EmptyLayout extends React.Component {
    
    render() {
        return (
            <div>
                <Background/>
                {this.props.children}
            </div>
        );
    }
}

export default EmptyLayout;