import React from 'react';
import Menubar from '../Menubar/Menubar';
import Toolbar from '../Toolbar/Toolbar';
import { useParams } from 'react-router-dom'

class MainLayout extends React.Component {
    state = {
    menuOpen: false,
    id:''
  };
  
  toggleClickHandler=()=>{
    this.setState((prev)=>{
        return {menuOpen: !prev.menuOpen}
    });
  };
    render() {
        let drawer='page';
        if(this.state.menuOpen)
            drawer='page move';
        return (
            
            <div>
                <Toolbar toggleClickHandler={this.toggleClickHandler}/>
                <Menubar show={this.state.menuOpen}/>
                <div className={drawer}>
                {this.props.children}
                </div>
            </div>
        );
    }
}

export default MainLayout;