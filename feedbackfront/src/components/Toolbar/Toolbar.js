import React from 'react'
import './Toolbar.css'
import ToggleMenu from '../Menubar/ToggleMenu'

class Toolbar extends React.Component {
    render() {
        return (
            <header className="toolbar">
                
                <nav className="toolbar-nav">
                    <div><ToggleMenu click={this.props.toggleClickHandler}/></div>
                    <div className="toolbar_title"><p>Feedback Transport Application</p></div>
                </nav>
                
            </header>
        );
    }
}

export default Toolbar;