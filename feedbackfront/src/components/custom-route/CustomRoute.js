import React from 'react';
import { Route } from 'react-router-dom';

class CustomRoute extends React.Component {

    render() {
        const { path, Component, Layout, ...rest } = this.props;
        return (
            <Route path={path} {...rest} render={(props) => (
                <Layout>
                    <Component {...props} />
                </Layout>
            )}/>
        );
    }
}

export default CustomRoute;