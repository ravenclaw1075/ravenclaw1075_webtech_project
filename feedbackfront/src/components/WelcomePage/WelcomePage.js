import React from 'react';
import './WelcomePage.css';
import $ from 'jquery';
import axios from 'axios';
import Table from 'react-bootstrap/Table'

class WelcomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            experiences: []
        };
    }
      componentDidMount(){
           axios.get('http://3.14.28.204:8080/api/routes/experiences').then(items=>{
            this.setState({
                experiences:items.data
            })
        });
        
          $("#arrow").click(function () {
                $('html,body').animate({
                    scrollTop: $("#my-section").offset().top
                }, 2000);
            });
      }
       
    render() {
        const experiences = () => {
            return this.state.experiences.map(experience => (
                <tr key={experience.id}>
                     <td>{experience.transportType}</td>
                     <td>{experience.publicTransport}</td>
                    <td>{experience.satisfactionLevel}</td>
                    <td>{experience.crowdnessLevel}</td>
                    <td>{experience.time}</td>
                    <td>{experience.duration}</td>
                    <td>{experience.startingPoint}</td>
                    <td>{experience.destinationPoint}</td>
                    <td>{experience.observations}</td>
                   
                </tr>
            )
        )};
        return (
            
      <div>
        <div className="txt-over">
            <h1>Welcome</h1>
            <p>This application was made by: RavenClaw Grupa 1075</p>
            <div className="btns">
                <a href="/login" className="btn btn-info">Login</a>
                <a href="/register" className="btn btn-info">Sign up</a>
                <a href="#" className="btn btn-default" id="arrow">Continue without Login</a>
            </div>
        </div>
            
       
        
        <div className="container-fluid section" id="my-section">
        <div className="container">
            <h1>Web application for sharing public transport experiences</h1>
            <p> Create your account and share your experience about the journey from A to B with our large collection of public transport information.
                Below, you can see other users experiences. Check this out!</p>

            <img className="img" src="https://www.gmv.com/export/sites/gmv/images/Transporte/SectoresCarretera_Landing.png"/>
            <h1>User experiences</h1>
            <Table responsive striped bordered hover variant="dark" style={{width:'fit-content', background:'rgba(3,24,45,0.8)'}}>
                   
                    <thead>
                        <tr>
                             <th>Transport Type</th>
                             <th>Public Transport</th>
                             <th>Satisfaction Level</th>
                             <th>Crowdness Level</th>
                             <th>Time</th>
                             <th>Duration</th>
                             <th>Starting Point</th>
                             <th>Destination Point</th>
                             <th>Observations</th>
                        </tr>
                        
                     </thead>
                     <tbody>
                         {experiences()}
                    
                    </tbody>
                   
            </Table>
        </div>
    </div>

        </div>
        
        );
    

    }
}
export default WelcomePage;