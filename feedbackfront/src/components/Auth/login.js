import React from 'react';
import './register.css';
import axios from 'axios';
import { withRouter,Redirect } from "react-router-dom";
import {NotificationContainer, NotificationManager} from 'react-notifications';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:'',
            id:''
        };
    }
   
    handleChangeUsername=(event)=>{
        this.setState({
            username:event.target.value
        });
    }
    handleChangePassword=(event)=>{
        this.setState({
           password:event.target.value 
        });
    }
    
    //aici trebuie un get dupa username sau ceva de genul (stiu ca era o chestie cu token dar nu mai stiu exact cum se facea) 
    login = (event) => {
        event.preventDefault();
            const username = this.state.username;
            const password=this.state.password;
            const user={username,password}

            axios.put('http://3.14.28.204:8080/api/routes/checklogin', user).then(result => {
             if(result.data.message==='wrong')
                    {
                        NotificationManager.error('Username or password incorrect!');
                    }
                else{
                    this.setState({
                        id:result.data.id
                    }, () => {this.props.history.push(`/myprofile/${result.data.id}`);});
                }
        });
        
    }

        
    render() {
        
        return (
            
            <div className="container login">
            
                <div className="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
          
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Login</h3>
                    </div>
                    <div className="panel-body">
                        <form>
                        
                        <div className="form-group">
                            <input className="form-control" placeholder="Username" name="username" type="text"
                            autoFocus="" required
                            onChange={this.handleChangeUsername}
                            value={this.state.username} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="Password" name="password" 
                            type="password" required
                            onChange={this.handleChangePassword}
                            value={this.state.password}/>
                        </div>
                        
                        <button className="submit-btn" onClick={this.login}>Login</button>
                        <div className="notif"><NotificationContainer/></div>
                    </form>
                     
                </div>
            </div>
            
        </div>
    </div>
           
        );
    }
}
export default withRouter(Login);