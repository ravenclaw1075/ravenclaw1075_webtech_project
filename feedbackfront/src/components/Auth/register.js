import React from 'react';
import './register.css';
import axios from 'axios';
import FormErrors from '../FormErrors/FormErrors';
import { withRouter,Redirect } from "react-router-dom";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email:'',
            birthDate:'',
            username:'',
            password:'',
            confirmpassword:'',
            formErrors: {email: '', birthDate:'', password: '', confirmpassword:''},
            emailValid: false,
            birthDateValid:false,
            passwordValid: false,
            confirmPasswordValid:false,
            formValid: false,
            toLogin:false
        };
    }
    handleChangeFirstName = (event) => {
        this.setState({
            firstName: event.target.value
        });
    }
    handleChangeLastName=(event)=>{
        this.setState({
           lastName:event.target.value 
        });
    }
    handleChangeEmail=(event)=>{
        this.setState({
            email:event.target.value
        });
    }
    handleChangeUsername=(event)=>{
        this.setState({
            username:event.target.value
        });
    }
    
    handleValidationInput =(event)=> {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value}, 
                () => { this.validateField(name, value) });
    }
    
  
    
    validateField(fieldName, value) {
        let password=this.state.password;
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let birthDateValid=this.state.birthDateValid;
        let passwordValid = this.state.passwordValid;
        let confirmPasswordValid=this.state.confirmPasswordValid;
        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case'birthDate':
                birthDateValid=value.match(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i);
                fieldValidationErrors.birthDate=birthDateValid?'' : ' this is not a date';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '': ' is too short';
            break;
            case'confirmpassword':
                confirmPasswordValid=value.length>=6 && value===password;
                fieldValidationErrors.confirmpassword=confirmPasswordValid? '':'Passwords not matching';
                break;
                
            default: break;
        }
        
        this.setState({formErrors: fieldValidationErrors,
                  emailValid: emailValid,
                  passwordValid: passwordValid,
                  confirmPasswordValid: confirmPasswordValid
            }, this.validateForm);
    }
    
    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid && this.state.confirmPasswordValid});
    }
    
      register = () => {
        const user = {
            FirstName: this.state.firstName,
            LastName: this.state.lastName,
            email:this.state.email,
            birthDate:this.state.birthDate,
            username:this.state.username,
            password:this.state.password
        };
       var post=false;
        axios.post('http://3.14.28.204:8080/api/routes/adduserdetail', user).then(
        res => {
             post=true;
        }).catch(err => {
        console.log(err);
        });
        this.setState({
           toLogin:true
        });
        
    }
        
    render() {
      if(this.state.toLogin){
              return (<Redirect
              to={{
                pathname: "/login",
                state: {
                  prevLocation: "/register",
                  error: "You need to login first!",
                },
              }}
            />);
      }
        else
        return  (
            <div className="container login">
            <div className="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                <div className="panel">
                    <div className="panel-heading">
                        <h3 className="panel-title">Register</h3>
                    </div>
                    <div className="panel-body">
                    
                    <div className="panel panel-default" style={{color:'red'}}>
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    
                        <form>
                            <div className="form-group">
                                <input className="form-control" placeholder="First Name" name="FirstName" type="text"
                                    autoFocus="" required
                                    onChange={this.handleChangeFirstName} value={this.state.firstName}/>
                            </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="Last Name" name="LastName" type="text"
                                autoFocus="" required
                                onChange={this.handleChangeLastName}
                                value={this.state.lastName} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="E-mail" name="email" type="email"
                            autoFocus="" required
                            onChange={this.handleValidationInput}
                            value={this.state.email} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="Birthdate: DD/MM/YYYY" name="birthDate" type="text"
                            autoFocus="" required
                            onChange={this.handleValidationInput}
                            value={this.state.birthDate} />
                        </div>
                        
                        <div className="form-group">
                            <input className="form-control" placeholder="Username" name="username" type="text"
                            autoFocus="" required
                            onChange={this.handleChangeUsername}
                            value={this.state.username} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" placeholder="Password" name="password" 
                            type="password" required
                            onChange={this.handleValidationInput}
                            value={this.state.password}/>
                        </div>
                        <div className="form-group">
                        
                            <input className="form-control" placeholder="Confirm Password" name="confirmpassword" type="password"
                            onChange={this.handleValidationInput}
                            value={this.state.confirmpassword} 
                                 required/>
                        </div>
                        <button className="submit-btn" disabled={!this.state.formValid} onClick={this.register}>Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
           
        );
    }
}
export default withRouter(Register);
