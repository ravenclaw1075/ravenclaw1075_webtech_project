import React, { Component } from 'react';

import './App.css'

//components
import AddExperience from './components/AddExperience/AddExperience';
import Register from './components/Auth/register';
import ExperiencesList from './components/Experiences/Experiences';
import Login from './components/Auth/login';
import UserProfile from './components/UserProfile/UserProfile';
import ChangePassword from './components/UserProfile/ChangePassword';
import WelcomePage from './components/WelcomePage/WelcomePage';


//Custom route+layouts
import CustomRoute from './components/custom-route/CustomRoute';
import RouteWithLayout from 'history'
import MainLayout from './components/layouts/MainLayout';
import EmptyLayout from './components/layouts/EmptyLayout';

import { BrowserRouter as Router, Switch } from 'react-router-dom';
//import axios from 'axios';

class App extends Component {
  
  render() {

    return (
      
      <div className='App'>
        
        <main style={{marginTop:'50px'}}>
        <Router>
        <Switch>
          
          <CustomRoute exact path="/" Layout={EmptyLayout} Component={WelcomePage}></CustomRoute>
          <CustomRoute path="/addexperience/:id" Layout={MainLayout} Component={AddExperience}></CustomRoute>
          <CustomRoute path="/myprofile/:id" Layout={MainLayout} Component={UserProfile}></CustomRoute>
          <CustomRoute path="/changepassword/:id" Layout={MainLayout} Component={ChangePassword}></CustomRoute>
          <CustomRoute path="/experiences/:id" Layout={MainLayout} Component={ExperiencesList}></CustomRoute>
          <CustomRoute path="/register" Layout={EmptyLayout} Component={Register}></CustomRoute>
          <CustomRoute path="/login" Layout={EmptyLayout} Component={Login}></CustomRoute>
        </Switch>
        </Router>
          
        </main>
      </div>
      
    );
  }
}
const Home=()=>(
  <div>
    <h1>First page</h1>
  </div>
  );
export default App;
